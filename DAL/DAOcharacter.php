<?php
namespace Beweb\Td\DAL;

use Beweb\Td\Models\Race;
use Beweb\Td\DAL\DAO;
use Beweb\Td\DAL\DAOJob;
use Beweb\Td\DAL\DAOraces;
use Beweb\Td\Models\Character;
use Beweb\Td\Models\Job;



class DAOcharacter extends DAO {




    function __construct(){
        $this->datasource = "../db/character.json";
    }

    // 



    /**
     * 
     * 
     */
    function load(): array{
        // Initialisation de mon tableau
        $characters = [];
        // j'insére dans une variable le contenus de mon fichier (get content)
        $datas = json_decode(file_get_contents($this->datasource),true);
        // boucle d'itération pour chaques clés dans mon tableau 
        foreach ($datas as  $character_as_array) {
            //  nouvel instance
            $c = new Character(new Race() , new Job() );
            $c->id = $character_as_array["id"];
            $c->race = $character_as_array["race"];
            $c->job = $character_as_array["job"];
            $c->stat = $character_as_array["stats"];

            array_push($characters,$c);
        }
        return $characters;
    }

/**
 * permet de cibler l'id d'un charactere dans un tableau associatif
 * 
 * 
 */
    function findById($id): mixed
    {
         //  on recupére la méthode qui elle même recupére les informations
 foreach ($this->load() as $character){

    if ($character['id'] == $id)

    {
        return $character;
    }
  }
  return false; 
    }


    /**
     * 
     * Permet de recupérer le nom indiquer dans des tableaux associatifs
     * 
     */
    function findByName($name): mixed
    {
         //  on recupére la méthode qui elle même recupére les informations
 foreach ($this->load() as $character){

     if ($character['id'] == $name)
     
     {
         return $character;
        }
    }
    return false;
}

/**
 * Je suis de bonne humeur : 
 * a vous de generer le bon id pour la data a persister 
 * 
 *
 * @param mixed $data
 * @return void
 */
function persist(mixed $data){
    $characters = $this->load();
    $data->id = count($characters) +1;
    array_push($characters,$data);
    file_put_contents($this->datasource,json_encode($characters));
}

/**
 * Ici on implémente une methode pour creer un personnage 
 * oui le DAO est un gestionnaire d'entité , il semble normal de lui deléguer 
 * la fastidieuse tache de fabriquer des personnages (oui ce serait con de faire ça dans l'index.php alors qu'on sait faire des objets ;) )
 * 
 * @todo faire en sorte de passer par un objet qui retourne le bon DAO selon ce qu'on veut faire
 * au lieu de les instanciers dans les methodes (cf : fabrique , singleton ).
 *
 * @param string $race idem que pour job mais pour  race , sauf que c'est différent parce que c'est pas pareil alors que les structures sont les mêmes, mais c'est tout a faire similaire malgré le corollaire évident de la propriété recherchée 
 * @param string $job la classe du personnage pour la chercher afin de l'ajouter a la création du personnage
 * @return Character le personnage que l'on vient de creer
 */
function createCharacter(string $race , string $job): Character{
    $daorace =  new DAOraces(); 
    $daojob =  new DAOJob(); 
    $c = new Character(
        $daorace->findByName($race),
        $daojob->findByName($job));
    return $c;
}
}
?>
