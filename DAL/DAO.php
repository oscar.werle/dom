<?php

namespace Beweb\Td\DAL ;


/**
 * Objet perméttant d'accéder a une source de donnée,
 * cet objet permet ne couche d'abstraction afin de facilité son utilisation 
 * 
 */

abstract class DAO {


    private String $datasource ; 


    
  abstract  public function persist($data);

  abstract  public function load(): mixed ;

  abstract public function findById($id): mixed ;

  abstract public function findByName($name): mixed ;

}
