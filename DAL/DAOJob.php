<?php 
namespace Beweb\Td\DAL;

use Beweb\Td\Models\Job;
use Beweb\Td\DAL\DAO;

class DAOJob extends DAO {

    // 
    function __construct(){
        $this->datasource = "../db/jobs.json";
    }

    // 
    function persist(mixed $data){
        json_decode(file_get_contents($this->datasource),true);
    }


    /**
     * 
     * 
     */
    function load(): array{
        // Initialisation de mon tableau
        $jobs = [];
        // j'insére dans une variable le contenus de mon fichier (get content)
        $datas = json_decode(file_get_contents($this->datasource),true);
        // boucle d'itération pour chaques clés dans mon tableau 
        foreach ($datas as  $job_as_array) {
            //  nouvel instance
            $j = new Job();
            $j->attack = $job_as_array["attack"];
            $j->defense = $job_as_array["defense"];
            $j->pv = $job_as_array["pv"];
            $j->name = $job_as_array["name"];
            $j->tcrit = $job_as_array["Tcrit"];
            $j->degcrit = $job_as_array["degCrit"];
            $j->id = $job_as_array["id"];
            array_push($jobs,$j);
        }
        return $jobs;
    }


    // chercher par nom dans mon tableau
  public  function findByName( $name): mixed
    {   

    //  on recupére la méthode qui elle même recupére les informations
    foreach ($this->load() as $job){

    if ($job->name == $name)

    {
        return $job;
    }
  }
  return new Job(); 
}


    function findById($id): mixed
    {
   
     //  on recupére la méthode qui elle même recupére les informations
    foreach ($this->load() as $job){

        if ($job->id == $id)
   
        {
            return $job;
        }
      }
      return false;
    
   }
 }

    