<?php 
namespace Beweb\Td\DAL;

use Beweb\Td\Models\Race;

class DAOraces extends DAO {


    function __construct(){
        $this->datasource = "../db/races.json";
    }

    // 
    function persist(mixed $data){
        return  json_encode(file_get_contents($this->datasource),true);
    }

    function load(): array{
        // Initialisation de mon tableau
        $races = [];
        // j'insére dans une variable le contenus de mon fichier (get content)
        $datas = json_decode(file_get_contents($this->datasource),true);
        // boucle d'itération pour chaques clés dans mon tableau 
        foreach ($datas as  $job_as_array) {
            //  nouvel instance
            $r = new Race;
            $r->name = $job_as_array["name"];
            $r->id = $job_as_array["id"];

            // je pousse dans mon tableau mes résulats ; 
            array_push($races,$r);
        }
        return $races;
    }



    function findById($id): mixed
    {
 //  on recupére la méthode qui elle même recupére les informations
 foreach ($this->load() as $race){

    if ($race->id == $id)

    {
        return $race;
    }
  }
  return new Race(); 
    }


    function findByName($name): mixed
    {
 //  on recupére la méthode qui elle même recupére les informations
    foreach ($this->load() as $race){

        if ($race->name == $name)
   
        {
            return $race;
        }
      }
      return new Race(); 
    }
}
