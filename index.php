
<?php

echo '<pre> <input type="button" value="Ouvrir" onClick="window.location.href=\'start.php\'">';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

echo "Hello !<br>";


// on récupère les variables globales pour ce script 
echo "script_name : ".$_SERVER['SCRIPT_NAME'] ;
echo "<br>";
echo "script_filename : ".$_SERVER['SCRIPT_FILENAME'] ; 
echo "<br>";

// on definit des constantes pour appeller les scripts qqsoit le directory
define('WEBROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_NAME']));
define('ROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_FILENAME']));

echo "ROOT : ".ROOT ;
echo "<br>";
echo "WEBROOT  : ".WEBROOT ; 
echo "<br>";
echo "main path : ".$mypath = ROOT.'core/controller.php' ;


// on affiche tout ce qui est passé 
var_dump($_GET);

// on recupere tous les paramètres de l'url sépares 
$param = explode('/',$_GET["p"]);
var_dump($param);

// require main abstract controller (cf autoload)  
require(ROOT.'core/controller.php');

// URL index.php/tuto/index 
// on extrait chaque parametre 
// le premier element nous donne l'objet controleur a appeller 
$controller = $param[0] ; // tuto
// le deuxieme l'action a effectuer 
$method = $param[1] ; // index 

//
$called = 'controllers/'.$controller.'.php' ;
require($called);

// on vérifie bien que la methode appellée existe dans la classe
if ( method_exists( $controller , $method )) {
    // on l'execute
    $myctrl = new $controller() ; // $myctrl = new Tuto(); 
    $myctrl->$method(); // $myctrl->index(); 
    // tuto->index()
}
else {
    echo "methode non existante, erreur 404";
}

